//Libraries
import { configureStore } from "@reduxjs/toolkit";

//Reducers
import signUpSlice from "../features/signupSlice/signUpSlice";
import validationSlice from "../features/validationSignup/validationSlice";
import setUpProfileSlice from "../features/setUpPetProfileSlice/setUpProfileSlice";
import loginSlice from "../features/loginSlice/loginSlice";
import setUpSellerProfileSlice from "../features/setUpSellerProfile/setUpSellerProfileSlice";
import serviceSlice from "../features/serviceSlice/serviceSlice";
import appointmentSlice from "../features/appointmentSlice/appointmentSlice";
import coordinateSlice from "../features/coordinateSlice/coordinateSlice";
import sellerSlice from '../features/fetchSellerInfos/fetchSellerInfos'
import editUserSlice from '../features/editInformation/editInformations'
import PetProfileSlice from '../features/fetchPetInformations/fetchPetInformations'
import editPetUserSlice from '../features/editPetInformations/editPetInformations'
import fetchDataSlice from '../features/fetchData/fetchData'
export default configureStore({
  reducer: {
    signUp: signUpSlice,
    signIn: loginSlice,
    setUpInfos: setUpProfileSlice,
    setUpSellerInfos: setUpSellerProfileSlice,
    validationRequest: validationSlice,
    service: serviceSlice,
    appointment: appointmentSlice,
    coordinates: coordinateSlice,
    getSellerInfo: sellerSlice,
    getPetProfileInfo: PetProfileSlice,
    editUser: editUserSlice,
    editPetUser: editPetUserSlice,
    fetchData: fetchDataSlice,
  },
});
