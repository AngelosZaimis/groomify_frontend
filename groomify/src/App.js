import React, { useEffect, useState } from "react";

//

//Route from react-router-dom
import { Route, Routes, useNavigate } from "react-router-dom";

// Sign up component
import Signup from "./components/signup/Signup";
import Login from "./components/login/Login";
import Navbar from "./components/navbar/navbar";
import Home from "./components/home/home";
import Dashboard from "./components/dashboard/dashboardSidebar";
import NotFound from "./components/404/NotFound";
import Footer from "./components/footer/footer";
import PetDashboard from "./components/petDashboard/PetDashboard";
import { addUser, addToken, addUserType } from "./features/loginSlice/loginSlice";

import { useSelector, useDispatch } from "react-redux";
import SetUpProfile from "./components/setUpProfile/SetUpProfile";

import { fetchPetProfileInfo } from "./features/fetchPetInformations/fetchPetInformations";

import { fetchSellerInfo } from "./features/fetchSellerInfos/fetchSellerInfos";

function App() {
  const dispatch = useDispatch();





  // Gets token and user Name and adds it in all components
  useEffect(() => {
    const storedToken = sessionStorage.getItem("token");
    const storedUsername = sessionStorage.getItem("username");
    const storedUserType = sessionStorage.getItem("user_type");

    
    if (storedToken) {
      dispatch(addToken(storedToken));
    
    }
    if (storedUsername) {
      dispatch(addUser(storedUsername));
    }

    if (storedUserType){
      dispatch(addUserType(storedUserType))
    }

    if(storedUserType === "pet_profile"){

      dispatch(fetchPetProfileInfo())
    }

    if(storedUserType === "seller_profile"){
      dispatch(fetchSellerInfo())
    }

  }, [dispatch]);

 

  return (
    <div className="App">
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/signup" element={<Signup />}></Route>
        <Route path="/login" element={<Login />}></Route>
        <Route path="/setupprofile" element={<SetUpProfile />} />
        <Route path="/dashboard" element={<Dashboard />}></Route>
        <Route path="/petdashboard" element={<PetDashboard/>}></Route>
        <Route path="*" element={<NotFound />}></Route>
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
