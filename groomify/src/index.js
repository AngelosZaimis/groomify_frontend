//React
import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter, Route, Routes, Link, Navigate } from "react-router-dom";
import { ChakraProvider } from "@chakra-ui/react";
import { ColorModeScript } from "@chakra-ui/color-mode";
//Redux
import { Provider } from "react-redux";
import store from "./store/store.js";
//styles
import theme from "./styles/theme";
//components
import App from "./App";


const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <BrowserRouter>
      <ChakraProvider theme={theme} >
        <ColorModeScript initialColorMode={theme.config.initialColorMode}/>
        <App />
      </ChakraProvider>
    </BrowserRouter>
  </Provider>
);
