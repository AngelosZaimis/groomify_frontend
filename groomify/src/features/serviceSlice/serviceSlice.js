import { createSlice } from "@reduxjs/toolkit";
import { useEffect } from "react";
import axios from "axios";

export const serviceSlice = createSlice({
  name: "service",
  initialState: {},
  reducers: {},
});

export default serviceSlice.reducer;
