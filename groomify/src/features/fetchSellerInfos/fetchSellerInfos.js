import { createSlice,createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";



export const fetchSellerInfo = createAsyncThunk(
    'seller/fetchSellerInfo',
    async() => {
        try {
            const response = await axios.get('https://groomify-backend.herokuapp.com/api/sellerprofile/infos/', {
                headers: {
                    'Authorization': `Bearer ${sessionStorage.getItem("token")}`, 
        
                },
                
            })
            return response.data
        }catch (error){
            throw error.response.data
        }
    }
)


const sellerSlice = createSlice({
    name: 'seller',
    initialState: {
      info: null,
      loading: false,
      error: null,
      username: "",
      email: "",
      first_name: "",
      last_name: "",
      city: "",
      company_name: "",
      company_address: "",
      country: "",
      dob: "",
      opening_time: "",
      closing_time: "",
      postcode: "",
      image: "",
    },
    reducers: {
      // add any other synchronous reducers here
    },
    extraReducers: (builder) => {
      builder
        .addCase(fetchSellerInfo.pending, (state) => {
          state.loading = true;
        })
        .addCase(fetchSellerInfo.fulfilled, (state,{payload: {info,username,email,first_name,last_name,city,company_address,company_name,country,dob,opening_time,closing_time,post_code,image,}}) => {
          state.loading = false;
          state.info = info
          state.username = username
          state.email = email
          state.first_name = first_name
          state.last_name = last_name
          state.city = city
          state.closing_time = closing_time
          state.opening_time = opening_time
          state.company_address = company_address
          state.company_name = company_name
          state.dob = dob
          state.country = country
          state.postcode = post_code
          state.image = image
    
        })
        .addCase(fetchSellerInfo.rejected, (state, action) => {
          state.loading = false;
          state.error = action.payload;
        });
    },
  });
  
  export default sellerSlice.reducer;