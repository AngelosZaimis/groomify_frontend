import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

export const setUpBooking = createAsyncThunk("setUpBooking", async (body) => {
  const response = await fetch(
    "https://groomify-backend.herokuapp.com/api/booking/",
    {
      method: "Post",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${sessionStorage.getItem("token")}`,
      },
      body: JSON.stringify(body),
    }
  );
  console.log(response)
  return response.json();
});

export const appointmentSlice = createSlice({
  name: "appointment",
  initialState: {
    service: "",
    date: "",
    time: "",
  },
  reducers: {},

  extraReducers: {
    [setUpBooking.pending]: (state, action) => {
      state.loading = true;
    },
    [setUpBooking.fulfilled]: (state, { payload: { error, message } }) => {
      state.loading = false;
      if (error) {
        state.errorValue = true;
        state.error = error;
      } else {
        state.message = message;
        state.success = true;
      }
    },
    [setUpBooking.rejected]: (state, action) => {
      state.loading = true;
    },
  },
});

export default appointmentSlice.reducer;
