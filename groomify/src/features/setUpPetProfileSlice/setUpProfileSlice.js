import { createSlice,createAsyncThunk } from "@reduxjs/toolkit";





export const setUpPetUser = createAsyncThunk('setUpPetUser', async(body) => {

    const formData = new FormData()
    formData.append("user",body.user)
    formData.append("first_name",body.first_name)
    formData.append("last_name",body.last_name)
    formData.append("your_pet",body.your_pet)
    formData.append("pet_name",body.pet_name)
    formData.append("pet_size",body.pet_size)
    formData.append("description",body.description)
    const petImage = body.pet_photo
    if (petImage){
        formData.append('pet_photo',body.pet_photo, body.pet_photo.name);
    }
    const response = await fetch("https://groomify-backend.herokuapp.com/api/petprofile/add-informations/",{
    
        method: "Post",
        headers: {
            'Authorization': `Bearer ${sessionStorage.getItem("token")}`, 

        },
        body: formData
    })
    return response.json()
})


const setUpUserSlice = createSlice({
    name: "setUpPetUser", 

    initialState: {
        username: "",
        firstname: "",
         


    },
    reducers:{

    },
    extraReducers: {
        [setUpPetUser.pending]: (state,action) => {
            state.loading = true
            
        },
        [setUpPetUser.fulfilled]: (state,{payload: {error, message}}) => {
            state.loading = false
            if (error) {
                state.errorValue = true
                state.error = error
            }else{
                state.message = message
                state.success = true
            }
        },
        [setUpPetUser.rejected]: (state,action) => {
            state.loading = true
          
        },

    }
})

export default setUpUserSlice.reducer
