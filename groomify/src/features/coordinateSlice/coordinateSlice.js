import { createSlice } from "@reduxjs/toolkit";

export const coordinateSlice = createSlice({
  name: "coordinate",
  initialState: {
    coordinates: {
      lat: null,
      lng: null,
    },
  },
  reducers: {
    setCoordinates: (state, action) => {
      state.coordinates = action.payload;
    },
  },
});

export const { setCoordinates } = coordinateSlice.actions;
export default coordinateSlice.reducer;
