import { createSlice,createAsyncThunk } from "@reduxjs/toolkit";





export const editUser = createAsyncThunk('editUser', async(body) => {
   
    const formData = new FormData()
    formData.append("username",body.username)
    formData.append("first_name",body.first_name)
    formData.append("last_name",body.last_name)
    formData.append("dob",body.dob)
    formData.append("company_name",body.company_name)
    formData.append("company_address",body.company_address)
    formData.append("opening_time",body.opening_time)
    formData.append("closing_time",body.closing_time)
    formData.append("city",body.city)
    formData.append("country",body.country)
    formData.append("postcode",body.postcode)
    const petImage = body.image
    if (petImage){
        formData.append('image',body.image, body.image.name);
    }
  
    const response = await fetch("https://groomify-backend.herokuapp.com/api/sellerprofile/add-informations/",{
    
        method: "PATCH",
        headers: {
            'Authorization': `Bearer ${sessionStorage.getItem("token")}`, 

        },
        body: formData
    })
    console.log(response); // add this line
    return response.json()
})


const editUserSlice = createSlice({
    name: "editUser", 

    initialState: {
        username: "",
        firstname: "",
         


    },
    reducers:{

    },
    extraReducers: {
        [editUser.pending]: (state,action) => {
            state.loading = true
            
        },
        [editUser.fulfilled]: (state,{payload: {error, message}}) => {
            state.loading = false
            if (error) {
                state.errorValue = true
                state.error = error
            }else{
                state.message = message
                state.success = true
            }
        },
        [editUser.rejected]: (state,action) => {
            state.loading = true
          
        },

    }
})

export default editUserSlice.reducer
