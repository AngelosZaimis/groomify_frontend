import { createSlice} from "@reduxjs/toolkit";



const fetchDataSlice = createSlice({
    name: "fetchDataSlice",

    initialState: {
        showCards: false
    },
    reducers: {
        showCard:(state)=> {
        
            state.showCards = true
            
        }
    } 
})


export default fetchDataSlice.reducer
export const {showCard} = fetchDataSlice.actions;