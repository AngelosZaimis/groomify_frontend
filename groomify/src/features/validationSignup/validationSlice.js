import { createSlice,createAsyncThunk } from "@reduxjs/toolkit";



export const validationRequest = createAsyncThunk('validationRequest', async(body) => {
    const response = await fetch('https://groomify-backend.herokuapp.com/api/auth/registration/validation/',{
    
        method: "Post",
        headers: {
            'Content-Type': "application/json",
        },
        body: JSON.stringify(body)
    })
    return await response.json()
})


const validationSlice = createSlice({
    name:"validationRequest",

    
    initialState:{
        code: "",
        message: "",
        success: false,
    },

    reducers: {
      
    },
    extraReducers:{
        [validationRequest.pending]: (state,action) => {
            state.loading = true
            state.success = false
        },
        [validationRequest.fulfilled]: (state,{payload: {error, email}}) => {
            state.loading = false
            if (error) {
                state.errorValue = true
                state.error = error
                state.success = false
            }else{
                state.message = email
                state.success = true
            }
        },
        [validationRequest.rejected]: (state,action) => {
            state.loading = true
            state.success = false
        },

    }

})



export default validationSlice.reducer
