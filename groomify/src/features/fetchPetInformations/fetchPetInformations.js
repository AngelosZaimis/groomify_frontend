import { createSlice,createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";



export const fetchPetProfileInfo = createAsyncThunk(
    'petprofile/fetchPetProfileInfo',
    async() => {
        try {
            const response = await axios.get('https://groomify-backend.herokuapp.com/api/petprofile/infos/', {
                headers: {
                    'Authorization': `Bearer ${sessionStorage.getItem("token")}`, 
        
                },
                
            })
     
            return response.data

        }catch (error){
            throw error.response.data
        }
          
    }
)


const PetProfileSlice = createSlice({
    name: 'petProfile',
    initialState: {
      info: null,
      loading: false,
      error: null,
      username: "",
      email: "",
      first_name: "",
      last_name: "",
      your_pet: "",
      pet_name: "",
      pet_size: "",
      description: "",
      your_appointments: null,
      pet_photo: "",
      id: ""
    
    },
    reducers: {
      // add any other synchronous reducers here
    },
    extraReducers: (builder) => {
      builder
        .addCase(fetchPetProfileInfo.pending, (state) => {
          state.loading = true;
        })
        .addCase(fetchPetProfileInfo.fulfilled, (state,{payload: {info,username,email,first_name,
             last_name,your_pet,pet_name,pet_size,description,your_appointments,pet_photo,id}}) => {
          state.loading = false;
          state.info = info
          state.username = username
          state.email = email
          state.first_name = first_name
          state.last_name = last_name
          state.your_pet = your_pet
          state.pet_name = pet_name
          state.pet_size = pet_size
          state.description = description
          state.your_appointments = your_appointments
          state.pet_photo = pet_photo
          state.id = id
        })
        .addCase(fetchPetProfileInfo.rejected, (state, action) => {
          state.loading = false;
          state.error = action.payload;
        });
    },
  });
  
  export default PetProfileSlice.reducer;