import { createSlice,createAsyncThunk } from "@reduxjs/toolkit";



export const signUpUser = createAsyncThunk('signupuser', async(body) => {
    const response = await fetch('https://groomify-backend.herokuapp.com/api/auth/registration/',{
    
        method: "Post",
        headers: {
            'Content-Type': "application/json",
        },
        body: JSON.stringify(body)
    })
    return await response.json()
})


const signUp = createSlice({
    name:"singupuser",

    
    initialState:{
        email: "",
        username: "",
        password: "",
        user_type: "",
        loading: false,
        message: "",
        success: false,
        errorValue: false,
        error: ""
    },

    reducers: {
        changeErrorValue: (state) =>{
            state.errorValue = true
        },
        changeErrorValueToFalse: (state) =>{
            state.errorValue = false
        }
    },
    extraReducers:{
        [signUpUser.pending]: (state,action) => {
            state.loading = true
            state.success = false
        },
        [signUpUser.fulfilled]: (state,{payload: {error, message}}) => {
            state.loading = false
            if (error) {
                state.errorValue = true
                state.error = error
                state.success = false
            }else{
                state.message = message
                state.success = true
            }
        },
        [signUpUser.rejected]: (state,action) => {
            state.loading = true
            state.success = false
        },

    }

})



export default signUp.reducer
export const {changeErrorValue, changeErrorValueToFalse} = signUp.actions