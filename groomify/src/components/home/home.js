import React from "react";
import Banner from "../banner/banner";
import Card from "../card/card";
import Map from "../map/map";
import { useSelector } from "react-redux";
import { Flex, Box, Center } from "@chakra-ui/react";

const Home = () => {


  const message = useSelector((state) => state.setUpSellerInfos.message)


  console.log(message)

  

  return (
    <div>
      <Banner />
      <Center>
        <Flex margin="5">
          <Box margin={(0, 0, 5, 5)}>
            <Card />
          </Box>
          <Map />
        </Flex>
      </Center>
    </div>
  );
};

export default Home;
