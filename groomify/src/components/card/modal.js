import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  Select,
  Input,
} from "@chakra-ui/react";
import { useDisclosure } from "@chakra-ui/hooks";
import React, { useEffect, useState} from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { setUpBooking } from "../../features/appointmentSlice/appointmentSlice";


const BookingModal = ({groomer}) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const finalRef = React.useRef(null);
  const [bookingData, setBookingData] = useState([]);
  const [date, setSelectedDate] = useState("");
  const [start_time, setSelectedTime] = useState("");
  const [service, setSelectedService] = useState("");
  const dispatch = useDispatch();
  const [seller,setSeller] = useState(groomer.id)
  const [User,setUser] = useState(
    useSelector((state) => state.getPetProfileInfo.email)
  )


  const submitBooking = async (e) => {
    e.preventDefault();
    dispatch(setUpBooking({
  
      User,
      seller,
      date,
      start_time,
      service,
    }))
  
    
    onClose()
  }  



  
  useEffect(() => {
   axios
    .get("https://groomify-backend.herokuapp.com/api/service/all/")
    .then((response) => {
      // Handle the response data
      console.log(response.data);
      setBookingData(response.data);
    })
    .catch((error) => {
      // Handle the error
      console.error(error);
    });
 
  }, []);

  

  const filteredServices = bookingData.filter((service) => service.seller_profile === groomer.id);



  return (
    <>
      <Button variant="solid" colorScheme="teal" onClick={onOpen}>
        Book Now
      </Button>
      <Modal finalFocusRef={finalRef} isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Booking Details</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
          <Select placeholder="Select Service" onChange={(e) => setSelectedService(e.target.value)}>
  {filteredServices.map((service) => (
    <option key={service.key} value={service.id}>
      {service.service}
    </option>
  ))}
</Select>
            <Input type='text' placeholder="Select Date" onChange={(e) => setSelectedDate(e.target.value)}>
            </Input>
            <Input type='text' placeholder="Select Time" onChange={(e) => setSelectedTime(e.target.value)}>
            </Input>
          </ModalBody>
          <ModalFooter>
            <Button colorScheme="red" mr={3} onClick={onClose}>
              Close
            </Button>
            <Button  variant="solid" colorScheme="teal" onClick={submitBooking}>
              Book
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default BookingModal;
