import {
  Card,
  CardBody,
  Image,
  Stack,
  Text,
  Heading,
  CardFooter,
  Box,
} from "@chakra-ui/react";
import React, {useEffect, useState } from "react";
import { StarIcon } from "@chakra-ui/icons";
import BookingModal from "./modal";
import axios from "axios";


import { useSelector } from "react-redux";


const Cards = () => {
  const [groomers, setGroomers] = useState([]);

  const fetchData = useSelector((state) => state.fetchData.showCards)

 
  console.log(fetchData)
  useEffect(() => {
    if (fetchData){
      axios
      .get("https://groomify-backend.herokuapp.com/api/sellerprofile/")
      .then((response) => {
        // Handle the response data
        console.log(response.data);
        setGroomers(response.data);
      })
      .catch((error) => {
        // Handle the error
        console.error(error);
      });
    }
  }, [fetchData]);

  const groomerCount = {
    reviewCount: 34,
    rating: 4,
  };
  return (
  
    <>
    {groomers.map((groomer) => (
      <Card
        key={groomer.id}
        direction={{ base: "column", sm: "row" }}
        width={{ base: "100%", sm: "100%" }}
        margin={{base: '8px'}}
        overflow="hidden"
        variant="outline"
       
      >
        <Image
          objectFit="cover"
          maxW={{ base: "100%", sm: "200px" }}
          src={groomer.image}
          alt="Shop Profile Image"
        />
        <Stack>
          <CardBody>
            <Heading size="md">{groomer.company_name}</Heading>
            <Text py="2">{groomer.city}</Text>
            <Box display="flex" mt="2" alignItems="center">
              {Array(5)
                .fill("")
                .map((_, i) => (
                  <StarIcon
                    key={i}
                    color={i < groomerCount.rating ? "teal.500" : "gray.300"}
                  />
                ))}
              <Box as="span" ml="2" fontSize="sm">
                {groomerCount.reviewCount} reviews
              </Box>
            </Box>
          </CardBody>

          <CardFooter>
            <BookingModal groomer={groomer} />
          </CardFooter>
        </Stack>
      </Card>
      ))}
    </>
  );
};

export default Cards;
