import React, { useState, useEffect } from "react";

import {
  Flex,
  Text,
  Box,
  Input,
  Stack,
  InputGroup,
  InputRightElement,
  Button,
  RadioGroup,
  Radio,
} from "@chakra-ui/react";

import { IoPaw } from "react-icons/io5";

// React dispatcher
import { useDispatch } from "react-redux";

// fetch funstion for request signup user slice
import { signUpUser } from "../../features/signupSlice/signUpSlice";

// validation request from validation slice
import { validationRequest } from "../../features/validationSignup/validationSlice";

// actions from signup slice
import {
  changeErrorValue,
  changeErrorValueToFalse,
} from "../../features/signupSlice/signUpSlice";

import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

export default function Signup() {
  const dispatch = useDispatch();

  const navigate = useNavigate();

  // For showing the pasword
  const [show, setShow] = React.useState(false);
  const handleClick = () => setShow(!show);

  // Redux selectors
  const messageFullfiled = useSelector((state) => state.signUp.message);
  const successfullRequest = useSelector((state) => state.signUp.success);
  const errorValue = useSelector((state) => state.signUp.errorValue);
  const successfullValidation = useSelector(
    (state) => state.validationRequest.message
  );
  const successfullVaidation = useSelector(
    (state) => state.validationRequest.success
  );

  // set loading icon
  const [isloading, setIsLoading] = useState(false);

  // use states for email, password, checkbox
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [user_type, setUserType] = useState("");
  const [required, setRequired] = useState(false);
  const [code, setValidationCode] = useState("");

  // Error Handling state
  const [invalidEmail, setInvalidEmail] = useState(false);
  const [invalidPassword, setInvalidPassword] = useState(false);
  const [invalidCode, setIndvalidCode] = useState(false);

  console.log(successfullValidation);

  // Function for registration
  const registerHandler = async (e) => {
    e.preventDefault();

    // Error handling
    if (
      email === "" ||
      username === "" ||
      password === "" ||
      user_type === ""
    ) {
      setRequired(true);
      return;
    }

    // Error handling
    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      dispatch(changeErrorValue());
      setInvalidEmail(true);
    }

    // Error handling
    if (password.length <= 6) {
      dispatch(changeErrorValue());
      setInvalidPassword(true);
    }

    dispatch(changeErrorValueToFalse());
    setInvalidEmail(false);
    setInvalidPassword(false);

    // Sending data to api through redux
    setIsLoading(true);
    await dispatch(
      signUpUser({
        email,
        username,
        password,
        user_type,
      })
    );
    setIsLoading(false);
  };

  // Validation function
  const validationHandler = async (e) => {
    e.preventDefault();

    if (code.length <= 0) {
      setIndvalidCode(true);
      return 0;
    }

    dispatch(validationRequest({ code }));
  };
  useEffect(() => {
    if (successfullRequest && !errorValue) {
      localStorage.removeItem("firstLogin");
    }
  }, [successfullRequest, errorValue]);

  // Login function navigates to login page
  const loginHandler = () => {
    navigate("/login");
  };

  useEffect(() => {}, [
    username,
    password,
    email,
    user_type,
    invalidEmail,
    invalidPassword,
  ]);

  return (
    <Flex
      width="100vw"
      height="90vh"
      justifyContent="center"
      alignItems="center"
      align="center"
    >
      {/* VALIDATION BOX  */}
      {
        // check if there is a successfull request and if there is one it gives a another component that and input for the code validation
        successfullRequest && !errorValue ? (
          <Box
            w={[300, 400, 500]}
            h="500"
            background="white"
            boxShadow="dark-lg"
            p="6"
            rounded="md"
            bg="white"
          >
            <Flex
              width="100%"
              height="100%"
              justifyContent="center"
              alignItems="center"
              flexDirection="column"
            >
              {successfullVaidation ? (
                <>
                  <Text fontSize="3xl" fontWeight="bold" marginBottom="30px">
                    Congratulations!
                  </Text>
                  <Text
                    textAlign="center"
                    fontSize="2xl"
                    marginBottom="40px"
                    color="black"
                  >
                    You have been verified
                  </Text>
                  <Text fontSize="xl" color="black">
                    Please click to login
                  </Text>
                  <Button
                    colorScheme="pink"
                    variant="solid"
                    marginTop="50px"
                    onClick={loginHandler}
                  >
                    Login
                  </Button>
                </>
              ) : (
                <>
                  <Text fontSize="3xl" fontWeight="bold" marginBottom="30px">
                    Congratulations!
                  </Text>
                  <Text
                    textAlign="center"
                    fontSize="2xl"
                    marginBottom="40px"
                    color="black"
                  >
                    {messageFullfiled}
                  </Text>
                  <Text fontSize="xl" color="black">
                    Please add the given code to verify your Email
                  </Text>
                </>
              )}
              {successfullVaidation ? (
                ""
              ) : (
                <Input
                  onChange={(e) => setValidationCode(e.target.value)}
                  placeholder="Code"
                  size="lg"
                  marginTop="20px"
                ></Input>
              )}
              {invalidCode && <p style={{ color: "red" }}>* Invalid Code</p>}
              {successfullVaidation ? (
                ""
              ) : (
                <Button
                  onClick={validationHandler}
                  marginTop="30px"
                  colorScheme="pink"
                  variant="solid"
                >
                  Submit
                </Button>
              )}
            </Flex>
          </Box>
        ) : (
          /* REGISTRATION BOX  */
          // othewise we get the registation component
          <Box
            w={[300, 400, 450]}
            h="600"
            boxShadow="dark-lg"
            p="6"
            rounded="md"
          >
            <Flex
              width="100%"
              height="100%"
              justifyContent="center"
              alignItems="center"
              flexDirection="column"
            >
              <Text
                color="#66bfbf"
                fontSize="3xl"
                fontWeight="bold"
                marginBottom="10px"
              >
                
                Welcome to Groomify 
         
              </Text>
             
              <Text
                color="#f76b8a"
                fontWeight="bold"
                marginBottom="50px"
                fontSize="2xl"
              >
                Sign up
              </Text>
    
              <Stack spacing={4} w={[200, 300, 350]} marginTop="30px">
                <Input
                  placeholder="Username"
                  onChange={(e) => setUsername(e.target.value)}
                  size="lg"
                />
                <Input
                  placeholder="Email address"
                  onChange={(e) => setEmail(e.target.value)}
                  size="lg"
                />
                {invalidEmail && (
                  <p style={{ color: "red" }}>* Enter a valid email address</p>
                )}
                {required && (
                  <p style={{ color: "red" }}>* Please add your email</p>
                )}
                <InputGroup size="md">
                  <Input
                    size="lg"
                    pr="4.5rem"
                    type={show ? "text" : "password"}
                    placeholder="Enter password"
                    errorBorderColor="red.300"
                    onChange={(e) => setPassword(e.target.value)}
                  />
                  <InputRightElement width="4.5rem">
                    <Button h="1.75rem" size="sm" onClick={handleClick}>
                      {show ? "Hide" : "Show"}
                    </Button>
                  </InputRightElement>
                </InputGroup>
                {invalidPassword && (
                  <p style={{ color: "red" }}>
                    * Password must be more than 6 integers
                  </p>
                )}
                {required && (
                  <p style={{ color: "red" }}>* Please add your password</p>
                )}
                <RadioGroup value={user_type} onChange={setUserType}>
                  <Radio marginRight="20px" value="seller_profile">
                    Bussiness Owner
                  </Radio>
                  <Radio value="pet_profile">Pet Owner</Radio>
                </RadioGroup>
                {required && (
                  <p style={{ color: "red" }}>* Please select a radio button</p>
                )}
                <Button
                  onClick={registerHandler}
                  isLoading={isloading}
                  loadingText="Submitting"
                  backgroundColor="#f76b8a"
                  color="#fcfefe"
                  variant="solid"
                  _hover={{ bg: "#66bfbf" }}
                >
                  Submit
                </Button>
              </Stack>
            </Flex>
          </Box>
        )
      }
    </Flex>
  );
}
