import React, { useState } from "react";
import { FormControl, Flex, Text, Box, Button, Input } from "@chakra-ui/react";

// React dispatcher
import { useDispatch, useSelector } from "react-redux";

import { useNavigate } from "react-router-dom";

// request function for signing in
import { signInRequest } from "../../features/loginSlice/loginSlice";

import { useEffect  } from "react";

import { useToast } from '@chakra-ui/react'

export default function Login() {
  const dispatch = useDispatch();

  const navigate = useNavigate();

  const toast = useToast();


  // set loading icon
  const [isloading, setIsLoading] = useState(false);

  // Selectors
  const userTypeSelector = useSelector((state) => state.signIn.user_type);
  const firstLoginSelector = useSelector((state) => state.signIn.first_login);

  // email and password
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // Submit function
  const handleSubmit = async(e) => {
    e.preventDefault();

    setIsLoading(true);
    await dispatch(signInRequest({ email, password }));
    setIsLoading(false);

  };

  useEffect(() => {
    if (firstLoginSelector) {
      navigate("/setupprofile");

    } else {
      if (userTypeSelector === "seller_profile") {
        navigate("/dashboard");
        toast({
          title: 'You have been logged in.',
          description: "Welcome to groomify.",
          status: 'success',
          duration: 3000,
          isClosable: true,
          position:"top-left"
        })
      } else if (userTypeSelector === "pet_profile") {
        navigate("/");
        toast({
          title: 'You have been logged in.',
          description: "Welcome to groomify.",
          status: 'success',
          duration: 3000,
          isClosable: true,
          position:"top-left"
        })
      }
    }
  }, [firstLoginSelector, userTypeSelector]);

  return (
    <div>
      <Flex
        width="100vw"
        height="90vh"
        justifyContent="center"
        alignItems="center"
        align="center"
      >
        <Box w={[300, 400, 450]} h="600" boxShadow="dark-lg" p="6" rounded="md">
          <Flex
            width="100%"
            height="100%"
            justifyContent="center"
            alignItems="center"
            flexDirection="column"
          >
            <Text
              color="#66bfbf"
              fontSize="3xl"
              fontWeight="bold"
              marginBottom="10px"
            >
              Welcome to Groomify
            </Text>
            
            <Text
              textAlign="center"
              color="#f76b8a"
              fontWeight="bold"
              marginBottom="50px"
              fontSize="2xl"
            >
              Log in
            </Text>
            <form onSubmit={handleSubmit}>
              <FormControl>
                <Input
                  height="50px"
                  w={[240, 350, 350]}
                  placeholder="Email"
                  type="email"
                  id="email"
                  name="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                ></Input>
              </FormControl>
              <FormControl mt={4}>
                <Input
                  height="50px"
                  w={[240, 350, 350]}
                  type="password"
                  id="password"
                  placeholder="Password"
                  name="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </FormControl>
              <Box display="flex" alignItems="center" justifyContent="center">
                <Button
                  w={[240, 350, 350]}
                  isLoading={isloading}
                  loadingText="Submitting"
                  backgroundColor="#f76b8a"
                  color="#fcfefe"
                  variant="solid"
                  _hover={{ bg: "#66bfbf" }}
                  mt={5}
                  type="submit"
                >
                  Submit
                </Button>
              </Box>
            </form>
          </Flex>
        </Box>
      </Flex>
    </div>
  );
}
