import React from "react";
import bannerImg from "../../assets/banner.jpg";
import { Button, Image, Box, Flex } from "@chakra-ui/react";
import { useColorMode } from "@chakra-ui/react";
import Autocomplete from "../autocomplete/autocomplete";

const Banner = () => {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <Flex justifyContent="center" flexDirection={["column", "column", "row"]}>
      {/* mobile */}
      <Image
        src={bannerImg}
        objectFit="cover"
        backgroundColor={"green"}
        display={["flex", "flex", "none"]}
      ></Image>
      <Box
        position={["relative", "relative", "absolute"]}
        backgroundColor={colorMode === "light" ? "gray.100" : "gray.900"}
        boxShadow="dark-lg"
        top={["0px", "0px", "150px"]}
        padding={["0px", "0px", "20px 60px"]}
        borderRadius={["0px", "0px", "70px"]}
      >
      
          <Autocomplete />

      
      </Box>
      {/* desktop */}
      <Image
        src={bannerImg}
        objectFit="cover"
        height={["100vh", "100vh", "650px"]}
        display={["none", "none", "flex"]}
      ></Image>
    </Flex>
  );
};
export default Banner;
