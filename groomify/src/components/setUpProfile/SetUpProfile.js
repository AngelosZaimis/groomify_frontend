import React from "react";
import {
  FormControl,
  FormLabel,
  Textarea,
  Input,
  Box,
  Flex,
  Text,
  Image,
  Button,
  Select,
} from "@chakra-ui/react";
import { useState } from "react";
import { useSelector } from "react-redux";

// async function for setting up the petprofile informations
import { setUpPetUser } from "../../features/setUpPetProfileSlice/setUpProfileSlice";

import { useNavigate } from "react-router-dom";

// async function for setting up the sellerprofile informations
import { setUpSellerUser } from "../../features/setUpSellerProfile/setUpSellerProfileSlice";

import { useDispatch } from "react-redux";

export default function SetUpProfile() {
  const navigate = useNavigate();

  const user_typeSelector = useSelector((state) => state.signIn.user_type);
  const usernameSelector = useSelector(
    (state) => state.signIn.username
  ).replace(/\"/g, "");

  const [username, setUsername] = useState("");
  const [first_name, setFirstName] = useState("");
  const [last_name, setLastName] = useState("");
  const [dob, setDob] = useState("");
  const [company_name, setCompanyName] = useState("");
  const [company_address, setCompanyAddress] = useState("");
  const [opening_time, setOpenTime] = useState("");
  const [closing_time, setCloseTime] = useState("");
  const [city, setCity] = useState("");
  const [country, setCountry] = useState("");
  const [postcode, setPostCode] = useState("");
  const [your_pet, setPetType] = useState("");
  const [pet_name, setPetName] = useState("");
  const [pet_size, setPetSize] = useState("");
  const [imageForShowing, setImageForShowing] = useState(
    "https://www.bedfordcentre.com/plugins/noveldesign-store-directory/images/default-shop.jpg"
  );
  const [pet_photo, setPetImage] = useState(null);
  const [image, setImage] = useState(null);
  const [description, setPetDescription] = useState("");
  const [isloading, setIsLoading] = useState(false);


  

  const dispatch = useDispatch();

  const submitHandler = async (e) => {
    e.preventDefault();

    setIsLoading(true);
    if (user_typeSelector === "seller_profile") {
      await dispatch(
        setUpSellerUser({
          username,
          first_name,
          last_name,
          dob,
          company_name,
          company_address,
          opening_time,
          closing_time,
          city,
          country,
          postcode,
          image,
        })
      );
    }

    if (user_typeSelector === "pet_profile") {
      await dispatch(
        setUpPetUser({
          first_name,
          last_name,
          your_pet,
          pet_name,
          pet_photo,
          pet_size,
          description,
          pet_photo,
        })
      );
    }
    setIsLoading(false);

    if (user_typeSelector === "seller_profile") {
      navigate("/dashboard");
    }

    if (user_typeSelector === "pet_profile") {
      navigate("/");
    }
  };

  return (
    <Flex
      mt={1}
      width="100%"
      justifyContent="center"
      alignItems="center"
      align="center"
    >
      <Box p={5} boxShadow="dark-lg">
        <Text textAlign="center" fontSize="4xl" fontWeight="bold">
          Welcome {usernameSelector}
        </Text>
        <Text textAlign="center" fontSize="1xl" marginBottom="20px" fontWeight="bold">
          Set up your profile
        </Text>
        {user_typeSelector === "seller_profile" ? (
          <FormControl marginTop="10px">
              <FormLabel htmlFor="image">Shop Image</FormLabel>
            <Image
              src={imageForShowing}
              w="120px"
              alt="User profile"
              marginBottom="20px"
            />
            <Input
              marginBottom="20px"
              id="image"
              w="120px"
              type="file"
              onChange={(e) => {
                setImage(e.target.files[0]);
                setImageForShowing(URL.createObjectURL(e.target.files[0]));
              }}
            />
          </FormControl>
        ) : null}
        <Flex display="grid" gridTemplateColumns="1fr 1fr" gridGap={9}>
          <FormControl>
            {user_typeSelector === "seller_profile" ? (
              <>
                <FormLabel htmlFor="username">Username</FormLabel>
                <Input
                  type="text"
                  id="username"
                  onChange={(e) => setUsername(e.target.value)}
                  placeholder="Enter your username"
                />
              </>
            ) :           <FormControl mt={4}>
            <FormLabel htmlFor="firstName">First Name</FormLabel>
            <Input
              type="text"
              id="firstName"
              onChange={(e) => setFirstName(e.target.value)}
              placeholder="Enter your first name"
            />
          </FormControl>}
          </FormControl>

          <FormControl mt={4}>
            <FormLabel htmlFor="lastName">Last Name</FormLabel>
            <Input
              type="text"
              id="lastName"
              onChange={(e) => setLastName(e.target.value)}
              placeholder="Enter your last name"
            />
          </FormControl>
          <FormControl mt={4}>
            {user_typeSelector === "seller_profile" ? (
              <>
                <FormLabel htmlFor="dob">Date of Birth</FormLabel>
                <Input
                  id="dob"
                  value={dob}
                  type="data"
                  onChange={(e) => setDob(e.target.value)}
                />
              </>
            ) : (
              <>
                <FormLabel htmlFor="pet_type">Pet type</FormLabel>
                <Input
                  id="pet_type"
                  onChange={(e) => setPetType(e.target.value)}
                />
              </>
            )}
          </FormControl>
          <FormControl mt={4}>
            {user_typeSelector === "seller_profile" ? (
              <>
                <FormLabel htmlFor="companyName">Company Name</FormLabel>
                <Input
                  type="text"
                  id="companyName"
                  onChange={(e) => setCompanyName(e.target.value)}
                  placeholder="Enter your company name"
                />
              </>
            ) : (
              <>
                <FormLabel htmlFor="pet_name">Pet name</FormLabel>
                <Input
                  id="pet_name"
                  onChange={(e) => setPetName(e.target.value)}
                />
              </>
            )}
          </FormControl>
          <FormControl mt={4}>
            {user_typeSelector === "seller_profile" ? (
              <>
                <FormLabel htmlFor="companyAddress">Company Address</FormLabel>
                <Input
                  type="text"
                  id="companyAddress"
                  onChange={(e) => setCompanyAddress(e.target.value)}
                  placeholder="Enter your company address"
                />
              </>
            ) : (
              <>
                <Select value={pet_size} onChange={(e) => setPetSize(e.target.value)} mb={4}>
                  <option value="Small">Small</option>
                  <option value="Medium">Medium</option>
                  <option value="Large">Large</option>
                </Select>
               </>
            )}
          </FormControl>
          <Flex mt={4}>
            <FormControl w="60%">
              {user_typeSelector === "seller_profile" ? (
                <>
                  <FormLabel htmlFor="openTime">Opening Time</FormLabel>
                  <Input
                    type="time"
                    value={opening_time}
                    onChange={(e) => setOpenTime(e.target.value)}
                    id="openTime"
                  />
                </>
              ) : (
                <>
                
                </>
              )}
            </FormControl>
            <FormControl w="48%" ml={4}>
              {user_typeSelector === "seller_profile" ? (
                <>
                  <FormLabel htmlFor="closeTime">Closing Time</FormLabel>
                  <Input
                    type="time"
                    value={closing_time}
                    onChange={(e) => setCloseTime(e.target.value)}
                    id="closeTime"
                  />
                </>
              ) : null}
            </FormControl>
          </Flex>
          <FormControl mt={4}>
            {user_typeSelector === "seller_profile" ? (
              <>
                <FormLabel htmlFor="city">City</FormLabel>
                <Input
                  type="text"
                  onChange={(e) => setCity(e.target.value)}
                  id="city"
                  placeholder="Enter your city"
                />
              </>
            ) : (
              <>
                <FormLabel htmlFor="description">Pet Description</FormLabel>
                <Textarea
                  height="200px"
                  id="description"
                  onChange={(e) => setPetDescription(e.target.value)}
                  name="description"
                />
              </>
            )}
          </FormControl>
          <FormControl mt={4}>
            {user_typeSelector === "seller_profile" ? (
              <>
                <FormLabel htmlFor="country">Country</FormLabel>
                <Input
                  type="text"
                  onChange={(e) => setCountry(e.target.value)}
                  id="country"
                  placeholder="Enter your country"
                />
              </>
            ) : <>  <FormLabel htmlFor="pet_image">Pet image</FormLabel>
            <FormLabel htmlFor="image-input">Upload Image</FormLabel>
            <Input
              w="120px"
              type="file"
              onChange={(e) => {
                setPetImage(e.target.files[0]);
                setImageForShowing(
                  URL.createObjectURL(e.target.files[0])
                );
              }}
            />

            <Image
              src={imageForShowing}
              w="120px"
              alt="User profile"
              marginBottom="10"
              marginTop={10}
            /></>}
          </FormControl>
          <FormControl mt={4}>
            {user_typeSelector === "seller_profile" ? (
              <>
                <FormLabel htmlFor="postCode">Post Code</FormLabel>
                <Input
                  type="text"
                  id="postCode"
                  onChange={(e) => setPostCode(e.target.value)}
                  placeholder="Enter your post code"
                />
              </>
            ) : null}
          </FormControl>
          <Button
            onClick={submitHandler}
            isLoading={isloading}
            loadingText="Submitting"
            colorScheme="pink"
            variant="solid"
          >
            Submit
          </Button>
        </Flex>
      </Box>
    </Flex>
  );
}
