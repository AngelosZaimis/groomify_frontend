import React, { useState, useEffect } from "react";
import GoogleMapReact from "google-map-react";
import { MdLocationOn } from "react-icons/md";
import { IoPaw } from "react-icons/io5";
import { Icon } from "@chakra-ui/icons";
import { Box, Text } from "@chakra-ui/react";
import { useSelector } from "react-redux";

import { Image, Center } from "@chakra-ui/react";

import Img from '../../assets/shop1.jpg'
import Img1 from '../../assets/shop3.png'
import Img2 from '../../assets/shop4.png'
import Img3 from '../../assets/shop2.jpg'
import Img4 from '../../assets/shop5.jpg'

let shopData = [
  {
    id: 1,
    name: "GroomingC",
    latitude: 47.37483389347178,
    longitude: 8.537677833366903,
    image: Img
  },

  {
    id: 2,
    name: "Dog Barber",
    latitude: 47.379628944548465,
    longitude: 8.549951621800398,
    image: Img1
  },
  {
    id: 3,
    name: "DogBuster",
    latitude: 47.3707356285987,
    longitude: 8.544266656507473,
    image: Img2
  },
  {
    id: 4,
    name: "PetLovers",
    latitude: 47.366840842292795,
    longitude: 8.536241487195374,
    image: Img3
  },
  {
    id: 5,
    name: "123grooming",
    latitude: 47.38276695731218,
    longitude: 8.540275529509177,
    image: Img4
  },
  // {
  //   id: 6,
  //   name: "Hot Dog",
  //   latitude: 47.37108440143446,
  //   longitude: 8.54928775182114,
  //   image: ""
  // },
];

const Map = () => {
  const coordinates = useSelector((state) => state.coordinates.coordinates);
  const [location, setLocation] = useState({
    latitude: coordinates.lat,
    longitude: coordinates.lng,
    shops: [],
    selectedShop: null,
  });

  useEffect(() => {
    setLocation({
      latitude: coordinates.lat,
      longitude: coordinates.lng,
      shops: shopData,
      selectedShop: null,
    });
  }, [coordinates]);

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        setLocation({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          shops: shopData,
          selectedShop: null,
        });
      },
      (error) => {
        console.error(error);
      }
    );
  }, []);

  return (
    <Box h="80vh" w="500px" display={["none", "none", "flex"]}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: "AIzaSyC1GuzW5OVhjXC5_9B2Vh8isebL5SLFlnM" }}
        defaultCenter={{
          lat: 47,
          lng: 8,
        }}
        center={{
          lat: location.latitude,
          lng: location.longitude,
        }}
        defaultZoom={14}
      >
        {location.shops.map((shop) => {
          return (
            <IoPaw
              color="black"
              size={20}
              key={shop.id}
              lat={shop.latitude}
              lng={shop.longitude}
              onClick={() => {
                setLocation({ ...location, selectedShop: shop });
           
              }}
            />
          );
        })}
        {location.selectedShop !== null && (
          <Box
            display="flex"
            flexDirection="column"
            alignItems="center"
            justifyContent="flex-start"
            background={"white"}
            width={150}
            height={160}
            borderRadius="15px"
            lat={location.selectedShop.latitude}
            lng={location.selectedShop.longitude}
            boxShadow="dark-lg"
          >
            <Image width="100%" height="130px" src={location.selectedShop.image}></Image>
            <Text color={"black"} fontSize="md" marginTop="5px">
              {location.selectedShop.name}
            </Text>
        
          </Box>
        )}

        <Icon
          as={MdLocationOn}
          color="red.500"
          boxSize={6}
          lat={location.latitude}
          lng={location.longitude}
        />
      </GoogleMapReact>
    </Box>
  );
};

export default Map;
