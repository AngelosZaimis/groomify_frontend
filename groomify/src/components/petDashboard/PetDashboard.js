import React, { useEffect } from "react";
import {
  Box,
  Text,
  Flex,
  Heading,
  Button,
  Input,
  Image,
} from "@chakra-ui/react";
import { useState } from "react";
import { useSelector } from "react-redux";
import { EditIcon, CloseIcon } from "@chakra-ui/icons";
import { useDispatch } from "react-redux";

import { editPetUser } from "../../features/editPetInformations/editPetInformations";

import { useToast } from '@chakra-ui/react'

export default function PetDashboard() {
  const dispatch = useDispatch();

  const toast = useToast()
  const [username, setUsername] = useState("");
  const [first_name, setFirstName] = useState("");
  const [last_name, setLastName] = useState("");
  const [your_pet, setPet] = useState("");
  const [pet_name, setPetName] = useState("");
  const [pet_size, setPetSize] = useState("");
  const [description, setDescription] = useState("");
  const [pet_photo, setPetPhoto] = useState("");
  const [showEditModal, setShowEditModal] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();

    dispatch(
      editPetUser({
        username,
        first_name,
        last_name,
        your_pet,
        pet_name,
        pet_size,
        description,
        pet_photo,
      })
    );
    setShowEditModal(false);
    toast({
      status:"info",
      title: 'New account information has been updated.',
      description: "",
      duration: 3000,
      isClosable: true,
      position:"top-left"
    })
  };
  const user = useSelector((state) => state.getPetProfileInfo);

  console.log(user.pet_photo)
  console.log(pet_photo)

  useEffect(() => {
    
  },[user])

  return (
    <Flex
      width="100%"
      justifyContent="center"
      alignItems="center"
      align="center"
    >
      <Box
        w={[300, 400, 600]}
        height="700px"
        borderWidth="1px"
        p={10}
        rounded="md"
      >
        <Flex justifyContent="space-between" alignItems="center">
          <Heading fontWeight="bold" mb="10">
            Welcome {user.username}
          </Heading>
          <Button
            onClick={() => setShowEditModal(!showEditModal)}
            leftIcon={showEditModal ? <CloseIcon /> : <EditIcon />}
            colorScheme="teal"
          >
            {showEditModal ? "Close" : "Edit"}
          </Button>
        </Flex>
        {showEditModal ? (
          <>
            <Text fontSize="3xl"> Edit your informations</Text>
            <Box height="800px" p="4">
            
              <Input
                size={"lg"}
                placeholder="First Name"
                value={first_name}
                onChange={(e) => setFirstName(e.target.value)}
                mb="6"
              />
              <Input
                size={"lg"}
                placeholder="Last Name"
                value={last_name}
                onChange={(e) => setLastName(e.target.value)}
                mb="6"
              />
              <Input
                size={"lg"}
                placeholder="Your Pet"
                value={your_pet}
                onChange={(e) => setPet(e.target.value)}
                mb="6"
              />
              <Input
                size={"lg"}
                placeholder="Pet Name"
                value={pet_name}
                onChange={(e) => setPetName(e.target.value)}
                mb="6"
              />
              <Input
                size={"lg"}
                placeholder="Pet Size"
                value={pet_size}
                onChange={(e) => setPetSize(e.target.value)}
                mb="6"
              />
              <Input
                size={"lg"}
                placeholder="Description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                mb="6"
              />
              <Text marginBottom="10px">Pet image:</Text>
              <Input
                type="file"
                accept="image/*"
                onChange={(e) => setPetPhoto(e.target.files[0])}
                mb="2"
              />
              <Button
                onClick={handleSubmit}
                type="submit"
                colorScheme="pink"
                variant="solid"
                mt={4}
              >
                Submit
              </Button>
            </Box>
          </>
        ) : (
          <>
            <Text marginBottom="30px">
              <strong>Username:</strong>
              {user.username}
            </Text>
            <Text marginBottom="30px">
              <strong>Email:</strong> {user.email}
            </Text>
            <Text marginBottom="30px">
              <strong>First Name:</strong> {user.first_name}
            </Text>
            <Text marginBottom="30px">
              <strong>Last Name:</strong> {user.last_name}
            </Text>
            <Text marginBottom="30px">
              <strong>Your Pet:</strong> {user.your_pet}
            </Text>
            <Text marginBottom="30px">
              <strong>Pet Name:</strong> {user.pet_name}
            </Text>
            <Text marginBottom="30px">
              <strong>Pet Size:</strong> {user.pet_size}
            </Text>
            <Text marginBottom="30px">
              <strong>Description:</strong> {user.description}
            </Text>
            <Text marginBottom="30px">
              <strong>Appointments:</strong> {user.appointments}
            </Text>
            <Text marginBottom="10px">
              <strong>Pet image:</strong>
            </Text>
            <Image w="160px" src={user.pet_photo}></Image>
          </>
        )}
      </Box>
    </Flex>
  );
}
