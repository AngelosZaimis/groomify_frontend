import * as React from "react";

const FacebookIcon = (props) => (
  <svg
    width={40}
    height={40}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M20 0C8.954 0 0 8.954 0 20s8.954 20 20 20 20-8.954 20-20S31.046 0 20 0Zm-1.478 20.936v7.494h3.016v-7.494h2.516l.377-2.921h-2.893V16.15c0-.846.235-1.422 1.448-1.422h1.546v-2.613A20.676 20.676 0 0 0 22.28 12c-2.23 0-3.757 1.361-3.757 3.86v2.155H16v2.92h2.522Z"
      fill="#fcfefe"
    />
  </svg>
);

export default FacebookIcon;
