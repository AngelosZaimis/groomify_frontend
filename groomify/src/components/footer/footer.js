import React from "react";
import { Flex, Box, Link as ChakraLink, Text } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import InstagramIcon from "./instIcon";
import TwitterIcon from "./twitIcon";
import FacebookIcon from "./facebookIcon";

const Footer = () => {
  return (
    <Flex
      marginTop='300px'
      flexDirection={["column", "column", "row"]}
      padding={["50px", "50px", "100px"]}
      display={["flex", "flex", "grid"]}
      gridTemplateColumns={"auto auto auto"}
      backgroundColor={"#66bfbf"}
    >
      <Box color={"#fcfefe"} >
        <Box marginBottom="5px">
          <Text as="b">GROOMIFY</Text>
        </Box>
        <Box>
          <Link to="/">About us</Link>
        </Box>
        <Box>
          <ChakraLink href="https://en.wikipedia.org/wiki/Terms_of_service">
            Terms of use
          </ChakraLink>
        </Box>
        <Box>
          <ChakraLink href="https://en.wikipedia.org/wiki/Privacy_policy">
            Privacy Policy
          </ChakraLink>
        </Box>
        <Box>
          <ChakraLink href="https://www.wikipedia.de/">Help Center</ChakraLink>
        </Box>
        <Box>
          <ChakraLink href="https://en.wikipedia.org/wiki/Blog">
            Blog
          </ChakraLink>
        </Box>
      </Box>
      <Box color={"#fcfefe"} marginTop={["10px", "10px", "0px"]}>
        <Box marginBottom="5px">
          <Text as="b">PET SERVICES</Text>
        </Box>
        <ChakraLink href="https://de.wikipedia.org/wiki/Grooming_(P%C3%A4dokriminalit%C3%A4t)">
          Pet Grooming
        </ChakraLink>
        <Box>
          <ChakraLink href="https://petsitting24.ch/">Pet Sitting</ChakraLink>
        </Box>
        <Box>
          <ChakraLink href="https://www.walking-dogs.ch/">
            Dog Walking
          </ChakraLink>
        </Box>
        <Box>
          <ChakraLink href="https://en.wikipedia.org/wiki/Pet_taxi">
            Pet Taxi
          </ChakraLink>
        </Box>
        <ChakraLink href="https://en.wikipedia.org/wiki/Dog_daycare">
          Pet Daycare
        </ChakraLink>
      </Box>
      <Box marginTop={["10px", "10px", "0px"]} color={"#fcfefe"}>
        <Box marginBottom="5px">
          <Text as="b">CONTACT US</Text>
        </Box>
        <Flex flexDirection="row" marginTop={["10px", "10px", "0px"]} gap="5px">
          <ChakraLink href="https://www.instagram.com/">
            <InstagramIcon />
          </ChakraLink>
          <ChakraLink href="https://de-de.facebook.com/">
            <TwitterIcon />
          </ChakraLink>
          <ChakraLink href="https://twitter.com/?lang=de">
            <FacebookIcon />
          </ChakraLink>
        </Flex>
      </Box>
    </Flex>
  );
};

export default Footer;
