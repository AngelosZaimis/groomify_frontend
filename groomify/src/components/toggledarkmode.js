import { Button } from "@chakra-ui/button";
import { useColorMode } from "@chakra-ui/react";
import { SunIcon, MoonIcon } from "@chakra-ui/icons";

const ToggleColormode = () => {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <Button onClick={() => toggleColorMode()}  m="1rem">
      {colorMode === "dark" ? <SunIcon/> : <MoonIcon/>}
    </Button>
  );
};

export default ToggleColormode;