import React, { useState } from "react";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from "react-places-autocomplete";
import { Input, Box, Button, Flex } from "@chakra-ui/react";
import { useDispatch} from "react-redux";
import { setCoordinates } from "../../features/coordinateSlice/coordinateSlice";
import { showCard } from "../../features/fetchData/fetchData";

const Autocomplete = () => {
  const [address, setAddress] = useState("");
  const dispatch = useDispatch();
  
 
  const handleAddressChange = (newAddress) => {
    setAddress(newAddress);
  };

  const handleSelect = async (address) => {
    const results = await geocodeByAddress(address);
    const latLng = await getLatLng(results[0]);
    console.log(latLng);
    dispatch(setCoordinates(latLng));
    dispatch(showCard())
  };



  return (
    <div>
      <PlacesAutocomplete value={address} onChange={handleAddressChange}>
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
          <Box>
            <Flex>
              <Input
                variant="outline"
                padding="30px"
                width={["100%", "70%", "400px"]}
                {...getInputProps({ placeholder: "Search Groomer close by" })}
              />
              <Button
                backgroundColor="#f76b8a"
                color="#fcfefe"
                variant="solid"
                padding="30px 60px"
                width={["100%", "30%", "auto"]}
                _hover={{ bg: "#66bfbf" }}
                onClick={() => handleSelect(address)}
              >
                Search
              </Button>
            </Flex>

            <Box>
              {loading ? <div>Loading...</div> : null}

              {suggestions.map((suggestion) => {
                return (
                  <Box {...getSuggestionItemProps(suggestion)}>
                    {suggestion.description}
                  </Box>
                );
              })}
            </Box>
          </Box>
        )}
      </PlacesAutocomplete>
    </div>
  );
};

export default Autocomplete;
