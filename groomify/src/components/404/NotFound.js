import React from "react";
import { Stack, Text, Button, Center } from "@chakra-ui/react";
import { ArrowBackIcon } from "@chakra-ui/icons";

const NotFound = () => {
  return (
    <Center>
      <Stack>
        <Text fontSize="6xl">404</Text>
        <Text fontSize="3xl">Oops! Page not found.</Text>
        <Text fontSize="2xl">Something went wrong. Please try again...</Text>
        <Button leftIcon={<ArrowBackIcon />} width="200px">
          Go Back
        </Button>
      </Stack>
    </Center>
  );
};

export default NotFound;
