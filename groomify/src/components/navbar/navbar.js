import {
  Button,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  IconButton,
  Flex,
  Box,
  Spacer,
  Center,
} from "@chakra-ui/react";
import { FcMenu } from "react-icons/fc";
import { Link } from "react-router-dom";
import ToggleColormode from "../toggledarkmode";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { logout } from "../../features/loginSlice/loginSlice";
import { useState } from "react";
import { IoPaw } from "react-icons/io5";
import { useToast } from '@chakra-ui/react'


const Navbar = () => {

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const toast = useToast();

  const [user_type, setUserType] = useState(() => {
    const storedUserType = sessionStorage.getItem("user_type");
    return storedUserType || ""; // set the initial state to the stored value or an empty string
  });
  
  const usernameSeller = useSelector((state) => state.getSellerInfo.username);

  const usernamePetProfile = useSelector(
    (state) => state.getPetProfileInfo.username
  );


  const tokenExists = sessionStorage.getItem("token")

 

  useEffect(() => {
    sessionStorage.setItem("user_type", user_type);
  }, [user_type,usernameSeller,usernamePetProfile,user_type]);


  

  const handleLogOut = () => {
    dispatch(logout());
    navigate("/");
    toast({
      status:"info",
      title: 'You have been logged out.',
      description: "Till next time.",
      duration: 3000,
      isClosable: true,
      position:"top-left"
    })
  };

  return (
    <Flex p="2" borderBottom="1px" borderColor="gray.300" padding="0 30px">
      <Center>
        <Box
          fontSize="3xl"
          color="teal.400"
          fontWeight="bold"
          display={"flex"}
          flexDirection={"row"}
          alignItems={"center"}
          gap={"10px"}
        >
          <Link to="/">Groomify</Link>
          <Link to="/">
            <IoPaw />
          </Link>
        </Box>
      </Center>
      <Spacer />
      {/* desktop */}
      <Box display={["none", "none", "flex"]} alignItems="center">
        {tokenExists ? (
          <Button variant="ghost" color="#f76b8a">
            <Link
              to={user_type === "pet_profile" ? "/petdashboard" : "dashboard"}
            >
              {user_type === "pet_profile"
                ? usernamePetProfile
                : usernameSeller}
            </Link>
          </Button>
        ) : (
          <Button variant="ghost" color="#f76b8a">
            <Link to="/signup">Sign up</Link>
          </Button>
        )}
        {tokenExists ? (
          <Button onClick={handleLogOut} variant="ghost" color="#f76b8a">
            Log out
          </Button>
        ) : (
          <Button variant="ghost" color="#f76b8a">
            <Link to="/login">Log in</Link>
          </Button>
        )}
        <ToggleColormode />
      </Box>
      {/* mobile */}
      <Box display={["flex", "flex", "none"]} alignItems="center">
        <Menu>
          <MenuButton
            as={IconButton}
            icon={<FcMenu />}
            variant="outlined"
            color="red.400"
          ></MenuButton>
          <MenuList>
            <MenuItem>
              <Link to="/signup">Sign up</Link>
            </MenuItem>
            <MenuItem>
              <Link to="/login">Log in</Link>
            </MenuItem>
          </MenuList>
        </Menu>
        <ToggleColormode />
      </Box>
    </Flex>
  );
};

export default Navbar;
