// Imports
import { useState, useEffect } from "react";

// Sidebar on left and Boxes with data on the right
import AppointmentBox from "./appointmentBox";
import ServiceBox from "./serviceBox";
import ProfileBox from "./profileBox";
import InfoBox from "./infoBox";

// Chakra Imports
import { Text, Divider, Heading, Flex } from "@chakra-ui/react";

// Icons
import { FiInfo, FiCalendar, FiUser } from "react-icons/fi";
import { BsShop } from "react-icons/bs";

import { IoPawOutline } from "react-icons/io5";

import { useSelector } from "react-redux";

// Sidebar items
import NavItem from "./navItem";

export default function Dashboard() {
  const [navSize, setNavSize] = useState("large");

  const shopname = useSelector((state) => state.getSellerInfo.company_name);

  // To show Components
  const [info, setInfo] = useState(true);
  const [appointment, setAppointment] = useState(false);
  const [service, setService] = useState(false);
  const [profile, setProfile] = useState(false);

  // Mobile resistance
  useEffect(() => {
    const handleWindowResize = () => {
      if (window.innerWidth <= 700) {
        setNavSize("small");
      } else if (window.innerWidth >= 700) {
        setNavSize("large");
      }
    };
    window.addEventListener("resize", handleWindowResize);

    return () => {
      window.removeEventListener("resize", handleWindowResize);
    };
  }, []);

  return (
    <div className="dashboard">
      <Flex w="100%" height={['120vh','200vh','200vh']}>
        <Flex
          w={navSize === "small" ? "17%" : "35%"}
          justifyContent={navSize === "small" ? "start" : "end"}
        >
          {/* Sidebar */}
          <div className="sidebar">
            <Flex w="100%" display="flex" justifyContent="center">
              <Flex
                display="flex"
                flexWrap="wrap"
                justifyContent="start"
              ></Flex>
            </Flex>
            <Flex
              pos="sticky"
              left="5"
              h="auto"
              boxShadow="0 4px 12px 0 rgba(0, 0, 0, 0.05)"
              borderRadius={navSize === "small" ? "15px" : "30px"}
              w={navSize === "small" ? "75px" : "230px"}
              flexDir="column"
              justifyContent="space-between"
              mr={5}
            >
              <Flex
                p="5%"
                flexDir="column"
                w="100%"
                alignItems={navSize === "small" ? "center" : "flex-start"}
                as="nav"
              >
                <NavItem
                  navSize={navSize}
                  icon={FiInfo}
                  title="Information"
                  isActive={info}
                  clickHandler={() => {
                    setInfo(true);
                    setAppointment(false);
                    setService(false);
                    setProfile(false);
                  }}
                />
                <NavItem
                  navSize={navSize}
                  icon={FiCalendar}
                  title="Appointments"
                  isActive={appointment}
                  clickHandler={() => {
                    setAppointment(true);
                    setService(false);
                    setProfile(false);
                    setInfo(false);
                  }}
                />
                <NavItem
                  active
                  navSize={navSize}
                  icon={IoPawOutline}
                  title="Services"
                  isActive={service}
                  clickHandler={() => {
                    setAppointment(false);
                    setService(true);
                    setProfile(false);
                    setInfo(false);
                  }}
                />
                <NavItem
                  navSize={navSize}
                  icon={FiUser}
                  title="Profile"
                  isActive={profile}
                  clickHandler={() => {
                    setAppointment(false);
                    setService(false);
                    setProfile(true);
                    setInfo(false);
                  }}
                />
              </Flex>
              <Flex
                p="5%"
                flexDir="column"
                w="100%"
                alignItems={navSize === "small" ? "center" : "flex-start"}
                mb={4}
              >
                <Divider display={navSize === "small" ? "none" : "flex"} />
                <Flex pl={3} mt={4} mr={3} align="center">
                  <BsShop size="35" color="#66bfbf" />
                  <Flex
                    flexDir="column"
                    ml={4}
                    mt={1.5}
                    display={navSize === "small" ? "none" : "flex"}
                  >
                    <Heading as="h3" size="sm">
                      ShopName
                    </Heading>
                    <Text color="gray">
                      {shopname ? <>{shopname}</> : "Add shopname"}
                    </Text>
                  </Flex>
                </Flex>
              </Flex>
            </Flex>
          </div>
        </Flex>
        <Flex>
          <Flex display="flex" flexWrap="wrap" justifyContent="start">
            {appointment && <AppointmentBox />}
            {service && <ServiceBox />}
            {profile && <ProfileBox />}
            {info && <InfoBox />}
          </Flex>
        </Flex>
      </Flex>
    </div>
  );
}
