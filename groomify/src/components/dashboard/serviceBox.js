import React from "react";
import { useState, useEffect } from "react";
import axios from "axios";

// chakra imports
import { Box, Heading, Button, Text } from "@chakra-ui/react";
import { AddIcon, CloseIcon } from "@chakra-ui/icons";

// Components
import Service from "./service";
import AddService from "./addService";

const ServiceBox = () => {
  const [addService, setAddService] = useState(false);
  const [services, setServices] = useState([]);

  // Fetch data
  useEffect(() => {
    axios
      .get("https://groomify-backend.herokuapp.com/api/service/me/", {
        headers: {
          Authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      })
      .then((response) => {
        setServices(response.data);
        console.log(response.data);
      })
      .catch((error) => console.error(error));
  }, [services]);

  return (
    <div className="serviceBox">
      <Box
        display="flex"
        flexDir="column"
        mb={4}
        mr={4}
        pos="sticky"
        boxShadow="0 4px 12px 0 rgba(0, 0, 0, 0.05)"
        borderRadius="30px"
        border="gray"
        justifyContent="start"
        p={10}
        minH={450}
      >
        <Heading>My Services ({services.length})</Heading>
        <Box display="flex" flexWrap="wrap" justifyContent="start" mt={10}>
          {addService && <AddService />}
          {services.map((service) => (
            <Service
              id={service.id}
              serviceName={service.service}
              description={service.description}
              price={service.price}
              duration={service.duration_in_hours}
              seller={service.seller}
            />
          ))}
        </Box>
        <Box>
          {services.length === 0 ? (
            <Text fontSize="20px" color="orange">
              You have no Services added yet!
            </Text>
          ) : null}
        </Box>
        <Box display="flex" flexDir="column" align="left" mt={5} w={200}>
          {!addService ? (
            <Button
              backgroundColor="#f76b8a"
              color="#fcfefe"
              variant="solid"
              padding="30px 60px"
              width={["100%", "100%", "auto"]}
              _hover={{ bg: "#66bfbf" }}
              onClick={() => setAddService(!addService)}
            >
              Add new Service &nbsp; <AddIcon />
            </Button>
          ) : (
            <Button
              backgroundColor="#f76b8a"
              color="#fcfefe"
              variant="solid"
              padding="30px 60px"
              width={["100%", "100%", "auto"]}
              _hover={{ bg: "#66bfbf" }}
              onClick={() => setAddService(!addService)}
            >
              Cancel action &nbsp; <CloseIcon />
            </Button>
          )}
        </Box>
      </Box>
    </div>
  );
};

export default ServiceBox;
