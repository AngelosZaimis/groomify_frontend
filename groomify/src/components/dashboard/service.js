import React from "react";
import axios from "axios";
import { useState } from "react";
import { DeleteIcon, CheckIcon } from "@chakra-ui/icons";

// Chakra imports
import {
  Box,
  Flex,
  Textarea,
  Card,
  CardHeader,
  CardBody,
  Stack,
  StackDivider,
  Heading,
  Button,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
} from "@chakra-ui/react";

const Service = ({ id, serviceName, description, price, duration }) => {
  // Text inputfields on new Appointment
  const [selectedService, setSelectedService] = useState(serviceName);
  const [descriptionInput, setDescriptionInput] = useState("");
  const [durationInput, setDurationInput] = useState("");
  const [priceInput, setPriceInput] = useState("");

  // Service dropdown
  const SERVICE_CHOICES = [
    { value: "Washing", label: "Washing" },
    { value: "Cutting and washing", label: "Cutting and washing" },
    { value: "Dental care", label: "Dental care" },
    { value: "Nail cutting", label: "Nail cutting" },
    { value: "Full service", label: "Full service" },
  ];

  const dropDownHandler = (event) => {
    setSelectedService(event.target.value);
  };

  // Button handlers
  const saveHandler = async (
    id,
    selectedService,
    descriptionInput,
    priceInput,
    durationInput
  ) => {
    try {
      const updatedFields = {};
      if (selectedService) {
        updatedFields.service = selectedService;
      }
      if (descriptionInput) {
        updatedFields.description = descriptionInput;
      }
      if (priceInput) {
        updatedFields.price = priceInput;
      }
      if (durationInput) {
        updatedFields.duration_in_hours = durationInput;
      }

      await axios.patch(
        `https://groomify-backend.herokuapp.com/api/service/${id}/`,
        updatedFields,
        {
          headers: {
            Authorization: `Bearer ${sessionStorage.getItem("token")}`,
          },
        }
      );
      console.log(`Service ${id} updated successfully.`);
    } catch (error) {
      console.error(`Error updating service ${id}:`, error);
    }
  };

  const deleteHandler = async () => {
    try {
      await axios.delete(
        `https://groomify-backend.herokuapp.com/api/service/${id}`,
        {
          headers: {
            Authorization: `Bearer ${sessionStorage.getItem("token")}`,
          },
        }
      );
      console.log(`Service ${id} deleted successfully.`);
    } catch (error) {
      console.error(`Error deleting service ${id}:`, error);
    }
  };

  return (
    <Box className="serviceBox">
      <Flex
        display="flex"
        flexDir="column"
        mb={4}
        mr={4}
        pos="sticky"
        left="5"
        borderRadius="5px"
        bg="#66bfbf"
        border="gray"
        justifyContent="center"
        p={1}
      >
        <Card w="300px">
          <CardHeader>
            <Heading size="md">{selectedService}</Heading>
          </CardHeader>

          <CardBody>
            <Stack divider={<StackDivider />} spacing="4">
              <Box>
                <Heading size="xs" textTransform="uppercase" align="left">
                  Service:
                </Heading>
                <select value={selectedService} onChange={dropDownHandler}>
                  {SERVICE_CHOICES.map((service) => (
                    <option key={service.value} value={service.value}>
                      {service.label}
                    </option>
                  ))}
                </select>
              </Box>
              <Box>
                <Heading size="xs" textTransform="uppercase" align="left">
                  Description:
                </Heading>
                <Textarea
                  size="md"
                  cols="5"
                  pt="2"
                  fontSize="sm"
                  onChange={(e) => setDescriptionInput(e.currentTarget.value)}
                >
                  {description}
                </Textarea>
              </Box>

              <Box>
                <Heading size="xs" textTransform="uppercase" align="left">
                  Duration in hours:
                </Heading>
                <NumberInput
                  precision={2}
                  step={0.5}
                  defaultValue={duration}
                  onChange={(valueString) => setDurationInput(valueString)}
                >
                  <NumberInputField />
                  <NumberInputStepper>
                    <NumberIncrementStepper />
                    <NumberDecrementStepper />
                  </NumberInputStepper>
                </NumberInput>
              </Box>
              <Box>
                <Heading size="xs" textTransform="uppercase" align="left">
                  Price:
                </Heading>
                <NumberInput
                  precision={2}
                  step={0.5}
                  defaultValue={price}
                  onChange={(valueString) => setPriceInput(valueString)}
                >
                  <NumberInputField />
                  <NumberInputStepper>
                    <NumberIncrementStepper />
                    <NumberDecrementStepper />
                  </NumberInputStepper>
                </NumberInput>
              </Box>
              <Box display="flex" justifyContent="space-between" mt={5}>
                <Button colorScheme="red" size="md" onClick={deleteHandler}>
                  Delete &nbsp;
                  <DeleteIcon />
                </Button>
                <Button
                  colorScheme="green"
                  size="md"
                  onClick={() =>
                    saveHandler(
                      id,
                      selectedService,
                      descriptionInput,
                      priceInput,
                      durationInput
                    )
                  }
                >
                  Save &nbsp;
                  <CheckIcon />
                </Button>
              </Box>
            </Stack>
          </CardBody>
        </Card>
      </Flex>
    </Box>
  );
};

export default Service;
