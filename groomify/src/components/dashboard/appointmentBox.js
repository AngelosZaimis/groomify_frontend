import React from "react";
import axios from "axios";
import { useState, useEffect } from "react";

// Chakra imports
import { Box, Heading, Text } from "@chakra-ui/react";

// Components
import Appointment from "./appointment";

const AppointmentBox = () => {
  const [appointments, setAppointments] = useState([]);

  // Fetch data
  useEffect(() => {
    axios
      .get("https://groomify-backend.herokuapp.com/api/booking/me/", {
        headers: {
          Authorization: `Bearer ${sessionStorage.getItem("token")}`,
        },
      })
      .then((response) => {
        setAppointments(response.data);
        console.log(response.data);
      })
      .catch((error) => console.error(error));
  }, []);

  return (
    <div className="serviceBox">
      <Box
        display="flex"
        flexDir="column"
        mb={4}
        mr={4}
        pos="sticky"
        left="5"
        boxShadow="0 4px 12px 0 rgba(0, 0, 0, 0.05)"
        borderRadius="30px"
        border="gray"
        justifyContent="start"
        p={10}
        minH={450}
      >
        <Heading>My Appointments ({appointments.length})</Heading>
        <Box display="flex" flexWrap="wrap" justifyContent="start" mt={10}>
          {appointments.length === 0 ? (
            <Text fontSize="20px" color="orange">
              You have no appointments in your inbox.
            </Text>
          ) : (
            appointments.map((appointment) => (
              <Appointment
                id={appointment.id}
                service={appointment.service}
                customer={appointment.user}
                date={appointment.date}
                startTime={appointment.start_time}
                endTime={appointment.end_time}
                confirmed={appointment.confirmed}
                timeOrdered={appointment.time_ordered}
              />
            ))
          )}
        </Box>
      </Box>
    </div>
  );
};

export default AppointmentBox;
