import React from "react";

// Chakra imports
import {
  Box,
  Flex,
  Card,
  CardHeader,
  CardBody,
  Stack,
  StackDivider,
  Heading,
  Text,
} from "@chakra-ui/react";

const Appointment = ({
  id,
  service,
  customer,
  date,
  startTime,
  endTime,
  confirmed,
  timeOrdered,
  services,
}) => {
  // Time ordered in right format
  const arr = timeOrdered.split("T");
  const d = arr[0];
  const t = arr[1].slice(0, -4);
  timeOrdered = d + " / " + t;

  return (
    <div className="appointments">
      <Flex
        display="flex"
        flexDir="column"
        mb={4}
        mr={4}
        pos="sticky"
        left="5"
        borderRadius="5px"
        border="gray"
        justifyContent="center"
        bg="#66bfbf"
        p={1}
      >
        <Card w="300px">
          <CardHeader>
            <Text>Date and Time</Text>
            <Heading size="md">
              {date} - {startTime.slice(0, -3)}
            </Heading>
          </CardHeader>

          <CardBody>
            <Stack divider={<StackDivider />} spacing="4">
              <Box>
                <Flex justifyContent="space-between">
                  <Heading size="xs" textTransform="uppercase" align="left">
                    Customer:
                  </Heading>
                  <Text size="md" fontSize="sm" border="none" type="text">
                    {customer}
                  </Text>
                </Flex>
              </Box>

              <Flex justifyContent="space-between">
                <Heading size="xs" textTransform="uppercase" align="left">
                  Service:
                </Heading>
                <Text size="md" fontSize="sm" border="none">
                  {service}
                </Text>
              </Flex>
              <Flex justifyContent="space-between">
                <Heading size="xs" textTransform="uppercase">
                  Date:
                </Heading>
                <Text size="md" fontSize="sm" border="none">
                  {date}
                </Text>
              </Flex>
              <Flex justifyContent="space-between">
                <Heading size="xs" textTransform="uppercase">
                  Start time:
                </Heading>
                <Text size="md" fontSize="sm" border="none">
                  {startTime.slice(0, -3)}
                </Text>
              </Flex>
              <Flex justifyContent="space-between">
                <Heading size="xs" textTransform="uppercase" align="left">
                  End time:
                </Heading>
                <Text size="md" fontSize="sm" border="none">
                  {endTime == null ? endTime : endTime.slice(0, -3)}
                </Text>
              </Flex>
              <Flex justifyContent="space-between">
                <Heading size="xs" textTransform="uppercase" align="left">
                  Confirmed:
                </Heading>
                <Text size="md" fontSize="sm">
                  {!confirmed ? "No" : "Yes"}
                </Text>
              </Flex>
              <Flex justifyContent="space-between">
                <Heading size="xs" textTransform="uppercase" align="left">
                  Ordered:
                </Heading>
                <Text size="md" fontSize="sm" border="none">
                  {timeOrdered}
                </Text>
              </Flex>
            </Stack>
          </CardBody>
        </Card>
      </Flex>
    </div>
  );
};

export default Appointment;
