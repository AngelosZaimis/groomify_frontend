import React from "react";
import { useSelector } from "react-redux";


// Chakra imports
import { Box, Heading, Flex, Text } from "@chakra-ui/react";

export default function InfoBox() {

  const username = useSelector((state)=> state.getSellerInfo.username)

  return (
    <div className="Informations">
      <Flex
        display="flex"
        flexDir="column"
        mr={4}
        pos="sticky"
        left="5"
        boxShadow="0 4px 12px 0 rgba(0, 0, 0, 0.05)"
        borderRadius="30px"
        border="gray"
        justifyContent="start"
        p={10}
        minH={450}
      >
        <Heading>Welcome {username}</Heading>
        <Flex flexDir="column" pt={10}>
          <Text fontSize="2xl">Thank you for choosing Groomify! 😊</Text>
          <br />
          <Text fontSize="xl">
            You are now on your Profile Dashboard. Here's how to get started:
          </Text>

          <Box mt={5}>
            <Text pb={2}>How to start:</Text>
            <Text pb={1}>
              1. Verify that your profile information is accurate by checking
              the left-hand Profile section.
            </Text>
            <Text pb={1}>
              2. Next, create all the services you offer in the Services
              section.
            </Text>
            <Text>
              3. Finally, view all the appointments made by your customers in
              the Appointments section.
            </Text>
          </Box>
        </Flex>
      </Flex>
    </div>
  );
}
