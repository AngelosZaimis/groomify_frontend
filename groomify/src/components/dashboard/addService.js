import React from "react";
import axios from "axios";
import { useState } from "react";
import { RiSendPlaneFill } from "react-icons/ri";

// Chakra imports
import {
  Box,
  Flex,
  Textarea,
  Card,
  CardHeader,
  CardBody,
  Stack,
  StackDivider,
  Heading,
  Button,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
} from "@chakra-ui/react";

export default function AddService() {
  // Text inputfields on new Appointment
  const [selectedService, setSelectedService] = useState("Washing");
  const [descriptionInput, setDescriptionInput] = useState("");
  const [durationInput, setDurationInput] = useState("");
  const [priceInput, setPriceInput] = useState("");

  // Service dropdown
  const SERVICE_CHOICES = [
    { value: "Washing", label: "Washing" },
    { value: "Cutting and washing", label: "Cutting and washing" },
    { value: "Dental care", label: "Dental care" },
    { value: "Nail cutting", label: "Nail cutting" },
    { value: "Full service", label: "Full service" },
  ];

  const dropDownHandler = (event) => {
    setSelectedService(event.target.value);
  };

  const createHandler = async (
    selectedService,
    descriptionInput,
    priceInput,
    durationInput
  ) => {
    try {
      await axios.post(
        "https://groomify-backend.herokuapp.com/api/service/",
        {
          service: selectedService,
          description: descriptionInput,
          price: String(Number(priceInput)),
          duration: String(Number(durationInput)),
        },
        {
          headers: {
            Authorization: `Bearer ${sessionStorage.getItem("token")}`,
          },
        }
      );
      console.log(`Service created successfully.`);
    } catch (error) {
      console.error(`Error creating service:`, error);
    }
  };

  return (
    <div className="addService">
      <div className="serviceBox">
        <Flex
          display="flex"
          flexDir="column"
          mb={4}
          mr={4}
          pos="sticky"
          left="5"
          borderRadius="5px"
          bg="#f76b8a"
          border="gray"
          justifyContent="center"
          p={1}
        >
          <Card w="300px">
            <CardHeader>
              <Heading size="md">New Service</Heading>
            </CardHeader>

            <CardBody>
              <Stack divider={<StackDivider />} spacing="4">
                <Box>
                  <Heading size="xs" textTransform="uppercase" align="left">
                    Service:
                  </Heading>
                  <select value={selectedService} onChange={dropDownHandler}>
                    {SERVICE_CHOICES.map((service) => (
                      <option key={service.value} value={service.value}>
                        {service.label}
                      </option>
                    ))}
                  </select>
                </Box>
                <Box>
                  <Heading size="xs" textTransform="uppercase" align="left">
                    Description:
                  </Heading>
                  <Textarea
                    size="md"
                    cols="5"
                    pt="2"
                    fontSize="sm"
                    onChange={(e) => setDescriptionInput(e.currentTarget.value)}
                  />
                </Box>

                <Box>
                  <Heading size="xs" textTransform="uppercase" align="left">
                    Duration in hours:
                  </Heading>
                  <NumberInput
                    precision={2}
                    step={0.5}
                    onChange={(valueString) => setDurationInput(valueString)}
                  >
                    <NumberInputField />
                    <NumberInputStepper>
                      <NumberIncrementStepper />
                      <NumberDecrementStepper />
                    </NumberInputStepper>
                  </NumberInput>
                </Box>
                <Box>
                  <Heading size="xs" textTransform="uppercase" align="left">
                    Price:
                  </Heading>
                  <NumberInput
                    precision={2}
                    step={0.5}
                    placeholder="Price"
                    onChange={(valueString) => setPriceInput(valueString)}
                  >
                    <NumberInputField />
                    <NumberInputStepper>
                      <NumberIncrementStepper />
                      <NumberDecrementStepper />
                    </NumberInputStepper>
                  </NumberInput>
                </Box>
                <Box display="flex" justifyContent="start" mt={5}>
                  <Button
                    colorScheme="green"
                    size="md"
                    onClick={() =>
                      createHandler(
                        selectedService,
                        descriptionInput,
                        priceInput,
                        durationInput
                      )
                    }
                  >
                    Create &nbsp; <RiSendPlaneFill />
                  </Button>
                </Box>
              </Stack>
            </CardBody>
          </Card>
        </Flex>
      </div>
    </div>
  );
}
