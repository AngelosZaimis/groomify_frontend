import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { EditIcon, CloseIcon } from "@chakra-ui/icons";
// Chakra imports
import {
  FormControl,
  FormLabel,
  Input,
  Box,
  Heading,
  Button,
  Text,
  VStack,
  Flex,
} from "@chakra-ui/react";

import { useNavigate } from "react-router-dom";
import { editUser } from "../../features/editInformation/editInformations";
import { useDispatch } from "react-redux";
import { useToast } from '@chakra-ui/react'


export default function ProfileBox() {
  const dispatch = useDispatch();

  // Seller data

  const toast = useToast()

  // STATES FOR EDITING DATA
  const [username, setUsername] = useState("");
  const [first_name, setFirstName] = useState("");
  const [last_name, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [dob, setBirthdate] = useState("");
  const [company_name, setCompanyName] = useState("");
  const [company_address, setCompanyAddress] = useState("");
  const [opening_time, setOpeningTime] = useState("");
  const [closing_time, setClosingTime] = useState("");
  const [city, setCity] = useState("");
  const [country, setCountry] = useState("");
  const [postcode, setPostcode] = useState("");
  const [image, setImage] = useState();
  const [isloading, setIsLoading] = useState(false);
  // state for showing the modal
  const [showEditModal, setShowEditModal] = useState(false);

  // selector for getting all infos from redux slice

  const handleSubmit = async (event) => {
    event.preventDefault();

    await dispatch(
      editUser({
        username,
        first_name,
        last_name,
        dob,
        company_name,
        company_address,
        opening_time,
        closing_time,
        city,
        country,
        postcode,
        image,
      })
    );

    setShowEditModal(false);
    toast({
      status:"info",
      title: 'New account information has been updated.',
      description: "",
      duration: 3000,
      isClosable: true,
      position:"top-left"
    })
  };

  const user = useSelector((state) => state.getSellerInfo);

  useEffect(() => {}, [user]);

  return (
    <div className="sellerInformations">
      <Box
        w={[300, 400, 600]}
        boxShadow="0 4px 12px 0 rgba(0, 0, 0, 0.05)"
        borderRadius="30px"
        border="gray"
        p={10}
        rounded="md"
      >
        <Flex justifyContent="space-between">
          <Heading size="xl" mb={7}>
            {showEditModal ? "Edit your Informations" : "User Information"}
          </Heading>
          <Button
            onClick={() => setShowEditModal(!showEditModal)}
            leftIcon={showEditModal ? <CloseIcon /> : <EditIcon />}
            colorScheme="green"
          >
            {showEditModal ? "Close" : "Edit"}
          </Button>
        </Flex>
        {
          /* EDIT COMPONENT */
          showEditModal ? (
            <Box maxW="md" mx="auto">
              <form onSubmit={handleSubmit}>
                <FormControl id="username" is>
                  <FormLabel>Username</FormLabel>
                  <Input
                    type="text"
                    value={username}
                    onChange={(event) => setUsername(event.target.value)}
                  />
                </FormControl>
                <FormControl id="firstName" is>
                  <FormLabel>First Name</FormLabel>
                  <Input
                    type="text"
                    value={first_name}
                    onChange={(event) => setFirstName(event.target.value)}
                  />
                </FormControl>
                <FormControl id="lastName">
                  <FormLabel>Last Name</FormLabel>
                  <Input
                    type="text"
                    value={last_name}
                    onChange={(event) => setLastName(event.target.value)}
                  />
                </FormControl>
                <FormControl id="email">
                  <FormLabel>Email</FormLabel>
                  <Input
                    type="email"
                    value={email}
                    onChange={(event) => setEmail(event.target.value)}
                  />
                </FormControl>
                <FormControl id="birthdate">
                  <FormLabel>Birthdate</FormLabel>
                  <Input
                    type="date"
                    value={dob}
                    onChange={(event) => setBirthdate(event.target.value)}
                  />
                </FormControl>
                <FormControl id="companyName">
                  <FormLabel>Company Name</FormLabel>
                  <Input
                    type="text"
                    value={company_name}
                    onChange={(event) => setCompanyName(event.target.value)}
                  />
                </FormControl>

                <FormControl id="companyAddress">
                  <FormLabel>Company Address</FormLabel>
                  <Input
                    type="text"
                    value={company_address}
                    onChange={(event) => setCompanyAddress(event.target.value)}
                  />
                </FormControl>
                <FormControl id="openingTime">
                  <FormLabel>Opening Time</FormLabel>
                  <Input
                    type="time"
                    value={opening_time}
                    onChange={(event) => setOpeningTime(event.target.value)}
                  />
                </FormControl>
                <FormControl id="closingTime">
                  <FormLabel>Closing Time</FormLabel>
                  <Input
                    type="time"
                    value={closing_time}
                    onChange={(event) => setClosingTime(event.target.value)}
                  />
                </FormControl>
                <FormControl id="city">
                  <FormLabel>City</FormLabel>
                  <Input
                    type="text"
                    value={city}
                    onChange={(event) => setCity(event.target.value)}
                  />
                </FormControl>
                <FormControl id="country">
                  <FormLabel>Country</FormLabel>
                  <Input
                    type="text"
                    value={country}
                    onChange={(event) => setCountry(event.target.value)}
                  />
                </FormControl>
                <FormControl id="postcode">
                  <FormLabel>Postcode</FormLabel>
                  <Input
                    type="text"
                    value={postcode}
                    onChange={(event) => setPostcode(event.target.value)}
                  />
                </FormControl>
                <FormControl id="image">
                  <FormLabel>Image</FormLabel>
                  <Input
                    type="file"
                    accept="image/*"
                    onChange={(event) => setImage(event.target.files[0])}
                  />
                </FormControl>
                <Button
                  type="submit"
                  isLoading={isloading}
                  loadingText="Submitting"
                  backgroundColor="#f76b8a"
                  color="#fcfefe"
                  variant="solid"
                  padding="30px 60px"
                  width={["100%", "100%", "auto"]}
                  _hover={{ bg: "#66bfbf" }}
                  mt={4}
                >
                  Submit
                </Button>
              </form>
            </Box>
          ) : (
            <VStack align="stretch" spacing={5}>
              {/* Informations  */}
              <Text>
                <strong>Username:</strong> {user.username}
              </Text>
              <Text>
                <strong>First Name:</strong> {user.first_name}
              </Text>
              <Text>
                <strong>Last Name:</strong> {user.last_name}
              </Text>
              <Text>
                <strong>Email:</strong> {user.email}
              </Text>
              <Text>
                <strong>Password:</strong> ********
              </Text>
              <Text>
                <strong>Birthdate:</strong> {user.dob}
              </Text>
              <Text>
                <strong>Company Name:</strong> {user.company_name}
              </Text>
              <Text>
                <strong>Company Address:</strong> {user.company_address}
              </Text>
              <Text>
                <strong>Opening Time:</strong> {user.opening_time}
              </Text>
              <Text>
                <strong>Closing Time:</strong> {user.closing_time}
              </Text>
              <Text>
                <strong>City:</strong> {user.city}
              </Text>
              <Text>
                <strong>Country:</strong> {user.country}
              </Text>
              <Text>
                <strong>Postcode:</strong> {user.postcode}
              </Text>
              <Text>
                <strong>Shop Images:</strong>{" "}
                <img
                  style={{ marginTop: "20px" }}
                  width="250px"
                  src={user.image}
                ></img>
              </Text>
            </VStack>
          )
        }
      </Box>
    </div>
  );
}
